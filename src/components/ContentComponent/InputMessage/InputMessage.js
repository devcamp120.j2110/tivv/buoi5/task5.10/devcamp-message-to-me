import { Component } from "react";

class InputMessage extends Component {
    render() {
        return (
            <>
            <label className='mt-3'>Message cho bạn 12 tháng tới: </label>
            <div className='col-12'>
            <input type='text' placeholder='Welcome to Devcamp120' className='form-control mb-3 w-75'></input>
            </div>
            <div className='col-12'>
            <button className='btn btn-success ' style={{width: "fit-content"}}>Gửi thông điệp</button>
            </div>
            <p className='text-primary pt-3'>Nội dung thông điệp</p>
            </>
        )
    }
}
export default InputMessage;