import { Component } from "react";
import Like from '../../../assets/images/Like.png'

class LikeImage extends Component {
    render() {
        return(
            <img alt='like.png' src={Like} className='w-25'></img>
        )
    }
}
export default LikeImage;