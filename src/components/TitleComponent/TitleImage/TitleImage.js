
import { Component } from "react";
import WelcomImage from '../../../assets/images/WelcomeToDevcamp.jpg';

class TitleImage extends Component {
    render() {
        return (
            <div className='col-12'>
            <img alt='WelcomeToDevcamp.jpg' src={WelcomImage} className='img-thumbnail w-75'></img>
            </div>
        )
    }
}
export default TitleImage;