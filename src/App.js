import 'bootstrap/dist/css/bootstrap.min.css';
import TitleText from './components/TitleComponent/TitleText/TitleText';
import TitleImage from './components/TitleComponent/TitleImage/TitleImage';
import InputMessage from './components/ContentComponent/InputMessage/InputMessage';
import LikeImage from './components/ContentComponent/LikeImage/LikeImage'

function App() {
  return (
    <div className="container">
      <div className='row pt-2 w-50'>
        <TitleText/>
        <TitleImage/>
        <InputMessage/>
        <LikeImage/>
      </div>
      
    </div>
  );
}

export default App;
